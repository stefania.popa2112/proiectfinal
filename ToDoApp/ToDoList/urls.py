from django.urls import path, include

from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.addToDoList, name='add'),
    path('complete/<todo_id>', views.completeToDoList, name='complete'),
    path('deletecomplete', views.deleteCompleted, name='deletecomplete'),
    path('deleteall', views.deleteAll, name='deleteall')

]