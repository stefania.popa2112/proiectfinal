from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST

from .models import ToDoList
from .forms import ToDoListForm

def index(request):
    todo_list = ToDoList.objects.order_by('id')

    form = ToDoListForm()

    context = {'todo_list' : todo_list, 'form' : form}

    return render(request, 'ToDoList/index.html', context)

@require_POST
def addToDoList(request):
    form = ToDoListForm(request.POST)

    if form.is_valid():
        new_todo = ToDoList(text=request.POST['text'])
        new_todo.save()

    return redirect('index')

def completeToDoList(request, todo_id):
    todo = ToDoList.objects.get(pk=todo_id)
    todo.complete = True
    todo.save()

    return redirect('index')

def deleteCompleted(request):
    ToDoList.objects.filter(complete__exact=True).delete()

    return redirect('index')

def deleteAll(request):
    ToDoList.objects.all().delete()

    return redirect('index')
