from django import forms

class ToDoListForm(forms.Form):
    text = forms.CharField(max_length=45,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Enter todo e.g. Delete junk files', 'aria-label': 'ToDoList',
                   'aria-describedby': 'add'}))
            #attrs={'class' : 'form-control', 'placeholder' : 'Enter what you want/need to do', 'aria-label' : 'ToDoList', 'aria-describedby' : 'add-btn'}))