Am realizat aplicatia numita NotesApp pornind de la cerintele date. 
Astfel, in aplicatia mea poti introduce notitele tale, care vor fi salvate in baza de date.
De asemenea, ele sunt afisate. Cand bifezi una din notite, aceasta este vazuta "completata" si devine bifata.
Am mai adaugat 2 butoane: "delete completed" si "delete all". "Delete completed" sterge notitele deja completate si
"Delete all" sterge toate notitele. 